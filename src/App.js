import React, { Component } from 'react';
import { BrowserRouter, Routes, Route } from 'react-router-dom';
import { ToastContainer } from 'react-toastify';
import UserInfo from './pages/UserInfo';
import CreateUser from './pages/CreateUser';
import EditUser from './pages/EditUser';
import Video from './pages/Video';

class App extends Component {
  render() {
    return (
      <>
        <BrowserRouter>
          <Routes>
            <Route path="/" element={<UserInfo />} />
            <Route path="/create-user" element={<CreateUser />} />
            <Route path="/edit/:id" element={<EditUser />} />
            <Route path="/video" element={<Video />} />
          </Routes>
        </BrowserRouter>
        <ToastContainer />
      </>
    );
  }
}

export default App;
