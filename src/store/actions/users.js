import { define } from '../redux-request';
import Api from '../../Api';

export const CREATE_USER = define('CREATE_USER');

export function createUserRequest(formData) {
  return CREATE_USER.request(() => Api.createUsers(formData));
}
export const GET_USERS = define('GET_USERS');

export function getUsersRequest() {
  return GET_USERS.request(() => Api.getAllUsers());
}

export const DELETE_USER = define('DELETE_USER');

export function deleteUserRequest(id) {
  return DELETE_USER.request(() => Api.deleteUser(id));
}

export const SINGLE_USER = define('SINGLE_USER');

export function singleUserRequest(id) {
  return SINGLE_USER.request(() => Api.getSingleUser(id));
}

export const UPDATE_USER = define('UPDATE_USER');

export function updateUserRequest(data) {
  return UPDATE_USER.request(() => Api.updateUser(data));
}
