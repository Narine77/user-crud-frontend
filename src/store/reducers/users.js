import { CREATE_USER, GET_USERS, SINGLE_USER } from '../actions/users';

const initialState = {
  users: [],
  usersStatus: '',
  singleUserStatus: '',
};

export default function reducer(state = initialState, action = {}) {
  switch (action.type) {
    case CREATE_USER.REQUEST: {
      return {
        ...state,
      };
    }
    case GET_USERS.REQUEST: {
      return {
        ...state,
        usersStatus: 'request',
      };
    }
    case GET_USERS.SUCCESS: {
      const { users } = action.payload.data;
      console.log(users, 'users');
      return {
        ...state,
        users,
        usersStatus: 'request',
      };
    }
    case GET_USERS.FAIL: {
      return {
        ...state,
        usersStatus: 'fail',
      };
    }
    case SINGLE_USER.REQUEST: {
      return {
        ...state,
        singleUserStatus: 'request',
      };
    }
    case SINGLE_USER.SUCCESS: {
      const { data } = action.payload.data;
      return {
        ...state,
        singleUserStatus: 'ok',
        data,
      };
    }
    case SINGLE_USER.FAIL: {
      return {
        ...state,
        singleUserStatus: 'fail',
      };
    }
    default: {
      return state;
    }
  }
}
