import React, { useState } from 'react';
import PropTypes from 'prop-types';
import _ from 'lodash';

function Input(props) {
  const [id] = useState(_.uniqueId('input'));
  const {
    error,
    label, iconLeft, className, iconRight, type, ...restProps
  } = props;

  return (
    <div className="input_wrapper">
      <label htmlFor={id}>{label}</label>
      {iconLeft ? <div className="input-icon input-icon-left">{iconLeft}</div> : null}
      <input
        className={`input ${className}`}
        id={id}
        type={type}
        {...restProps}
      />
      {error ? <span className="error">{error}</span> : null}
      {iconRight ? <div className="input-icon input-icon-right">{iconRight}</div> : null}
    </div>
  );
}

Input.propTypes = {
  className: PropTypes.string,
  label: PropTypes.string,
  iconLeft: PropTypes.element,
  iconRight: PropTypes.element,
  type: PropTypes.string,
  error: PropTypes.string,
};

Input.defaultProps = {
  className: '',
  iconLeft: undefined,
  iconRight: undefined,
  label: '',
  error: '',
  type: 'text',
};
export default Input;
