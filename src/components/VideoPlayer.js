import React, { memo, useRef } from 'react';

import PropTypes from 'prop-types';

export default memo(({ src, onPlay, onPause }) => {
  const videoRef = useRef();
  const countRef = useRef(0);
  countRef.current++;
  console.log(countRef);
  return (
    <div>
      <p>
        Count
        {countRef.current}
      </p>
      <h2>{src.title}</h2>
      <video src={src.url} ref={videoRef} />
      <button
        type="button"
        onClick={() => {
          videoRef.current.play();
          onPlay();
        }}
      >
        Play
      </button>
      <button
        type="button"
        onClick={() => {
          videoRef.current.pause();
          onPause();
        }}
      >
        Pause
      </button>
    </div>
  );
},
// (prevProps, nextProps) => {
//   if (prevProps.onPlay !== nextProps.onPlay) { return false; }
//   if (prevProps.onPause !== nextProps.onPause) return false;
//   if (prevProps.src.title !== nextProps.src.title) { return false; }
//   if (prevProps.src.url !== nextProps.src.url) { return false; }
//   return true;
// }
);
