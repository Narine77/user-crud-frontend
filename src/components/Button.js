import React from 'react';
import PropTypes from 'prop-types';

function Button(props) {
  const
    {
      className, type, icon, title, ...restProps
    } = props;
  return (
    <button className={`btn ${className}`} type={type} {...restProps}>
      { icon || null }
      { title || null }
    </button>
  );
}
Button.propTypes = {
  className: PropTypes.string,
  title: PropTypes.string,
  icon: PropTypes.element,
  type: PropTypes.string,
};

Button.defaultProps = {
  className: '',
  title: '',
  icon: undefined,
  type: 'text',
};
export default Button;
