import axios from 'axios';

const api = axios.create({
  baseURL: 'http://localhost:4001',
});

// api.interceptors.request.use((config) => {
//   const token = localStorage.getItem('token');
//   if (token) {
//     config.headers.Authorization = `Bearer${token}`;
//   }
//   return config;
// }, Promise.resolve);

const toFormData = (data, indices = false) => serialize({ ...data }, { indices });
class Api {
  // static createUsers(data) {
  //   return api.post('users/user-register', toFormData(data));
  // }
  static createUsers(data) {
    return api.post('users/add-user', data);
  }

  static getAllUsers() {
    return api.get('users/users-list');
  }

  static deleteUser(id) {
    return api.delete(`/users/delete/${id}`);
  }

  static getSingleUser(id) {
    return api.get(`/users/single-user/${id}`);
  }

  static updateUser(data) {
    return api.put('/users/update-user', data);
  }
}
export default Api;
