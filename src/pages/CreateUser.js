import React, { useState } from 'react';
import _ from 'lodash';
import { useDispatch } from 'react-redux';
import { toast } from 'react-toastify';
import { useNavigate } from 'react-router-dom';
import Input from '../components/Input';
import Button from '../components/Button';
import { createUserRequest, getUsersRequest } from '../store/actions/users';

function CreateUser(props) {
  const navigate = useNavigate();
  const dispatch = useDispatch();
  const [formData, setFormData] = useState({});
  const [errors, setErrors] = useState({});
  const keys = ['name', 'lName', 'email', 'phone', 'profession'];

  const handleChange = (key, value) => {
    formData[key] = value;
    _.set(formData, key, value);
    setFormData({ ...formData });
    _.unset(formData, keys);
  };
  const handleSubmit = async (ev) => {
    ev.preventDefault();
    const { payload: { data } } = await dispatch(createUserRequest(formData));
    if (data.status === 'error') {
      toast.error('Invalid data!');
    } else {
      toast.success('Successfully created!');
      setFormData('');
      setTimeout(() => {
        navigate('/');
      }, 1500);
    }
  };
  return (
    <div className="create-user">
      <div className="container">
        <p className="create-user-text">Create User</p>
        <form className="create-user-block" onSubmit={handleSubmit}>
          <Input
            label="First Name"
            onChange={(ev) => handleChange('name', ev.target.value)}
            value={formData.name || ''}
          />
          <Input
            label="Last Name"
            onChange={(ev) => handleChange('lName', ev.target.value)}
            value={formData.lName || ''}
          />
          <Input
            label="Email"
            onChange={(ev) => handleChange('email', ev.target.value)}
            value={formData.email || ''}
            // error={errors.email ? 'Please enter valid email' : null}
          />
          <Input
            label="Phone"
            onChange={(ev) => handleChange('phone', ev.target.value)}
            value={formData.phone || ''}
          />
          <Input
            label="Profession"
            onChange={(ev) => handleChange('profession', ev.target.value)}
            value={formData.profession || ''}
          />
          <Button
            title="Add user"
            onClick={handleSubmit}
            disabled={keys.some((k) => _.isEmpty((formData[k] || '')))}
          />
        </form>
      </div>
    </div>
  );
}

export default CreateUser;
