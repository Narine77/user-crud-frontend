import React, { useMemo, useState } from 'react';
import _ from 'lodash';
import { useDispatch } from 'react-redux';
import { useLocation, useNavigate, useParams } from 'react-router-dom';
import { toast } from 'react-toastify';
import Input from '../components/Input';
import Button from '../components/Button';
import { singleUserRequest, updateUserRequest } from '../store/actions/users';

function EditUser(props) {
  const navigate = useNavigate();
  const dispatch = useDispatch();
  const [formData, setFormData] = useState({});
  const { id } = useParams();
  const location = useLocation();
  console.log(id, 'id');
  const keys = ['name', 'lName', 'email', 'phone', 'profession'];

  useMemo(() => {
    if (id) {
      console.log(id, 'uId');
      (async () => {
        const { payload: { data } } = await dispatch(singleUserRequest(id));
        console.log(data, 'data');
        setFormData(data.singleUser);
      })();
    }
  }, [id]);
  const handleChange = (key, value) => {
    formData[key] = value;
    _.set(formData, key, value);
    setFormData({ ...formData });
    // _.unset(formData, keys);
  };
  const handleSubmit = async (ev) => {
    ev.preventDefault();
    const { payload: { data } } = await dispatch(updateUserRequest(formData));
    if (data.status === 'error') {
      toast.error('Invalid data!');
    } else {
      toast.success('Successfully edited!');
      setFormData('');
      setTimeout(() => {
        navigate('/');
      }, 1500);
    }
  };
  return (
    <div className="edit-user">
      <div className="container">
        <p className="edit-user-text">Edit User</p>
        <form className="edit-user-block" onSubmit={handleSubmit}>
          <Input
            label="First Name"
            onChange={(ev) => handleChange('name', ev.target.value)}
            value={formData.name || ''}
          />
          <Input
            label="Last Name"
            onChange={(ev) => handleChange('lName', ev.target.value)}
            value={formData.lName || ''}
          />
          <Input
            label="Email"
            onChange={(ev) => handleChange('email', ev.target.value)}
            value={formData.email || ''}
          />
          <Input
            label="Phone"
            onChange={(ev) => handleChange('phone', ev.target.value)}
            value={formData.phone || ''}
          />
          <Input
            label="Profession"
            onChange={(ev) => handleChange('profession', ev.target.value)}
            value={formData.profession || ''}
          />
          <Button
            title="Edit user"
            onClick={handleSubmit}
            disabled={keys.some((k) => _.isEmpty((formData[k] || '')))}
          />
        </form>
      </div>
    </div>
  );
}

export default EditUser;
