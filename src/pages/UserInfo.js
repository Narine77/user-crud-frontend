import React, { useEffect, useState } from 'react';
import { Link } from 'react-router-dom';
import { useDispatch, useSelector } from 'react-redux';
import Button from '../components/Button';
import { deleteUserRequest, getUsersRequest } from '../store/actions/users';

function UserInfo(props) {
  const dispatch = useDispatch();
  const { users } = useSelector((state) => state.users);

  useEffect(() => {
    dispatch(getUsersRequest());
  }, []);

  const handleDelete = async (ev) => {
    dispatch(deleteUserRequest(ev.target.id));
    dispatch(getUsersRequest());
  };
  return (
    <div className="user-info">
      <div className="container">
        <p className="user-info-text">User Info</p>
        <div className="create-user">
          <p className=" btn create-user-btn"><Link to="/create-user">Create User</Link></p>
        </div>
        <div className="user-info-element head">
          <div className="user-info-block">
            <div className="id">ID</div>
            <div className="name">Name</div>
            <div className="last-name">Last Name</div>
            <div className="email">Email</div>
            <div className="phone">Phone</div>
            <div className="speciality">Profession</div>
          </div>
          <div className="button-block">
            <p className="actions">Actions</p>
          </div>
        </div>
        {users.map((u) => (
          <div className="user-info-element body">
            <div className="user-info-block">
              <div className="id">{u.id}</div>
              <div className="name">{u.name}</div>
              <div className="last-name">{u.lName}</div>
              <div className="email">{u.email}</div>
              <div className="phone">{u.phone}</div>
              <div className="speciality">{u.profession}</div>
            </div>
            <div className="button-block">
              <Link to={`/edit/${u.id}`}><Button type="button" className="edit" title="Edit" /></Link>
              <Button id={u.id} type="button" className="delete" onClick={handleDelete} title="Delete" />
            </div>
          </div>
        ))}
      </div>
    </div>
  );
}

export default UserInfo;
