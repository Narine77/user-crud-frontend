import React, { useCallback, useMemo, useState } from 'react';
import VideoPlayer from '../components/VideoPlayer';

function Video() {
  const [text, setText] = useState('');
  const [isPlaying, setIsPlaying] = useState(false);
  const onPlay = useCallback(() => {
    setIsPlaying(true);
  }, [setIsPlaying]);
  const onPause = useCallback(() => {
    setIsPlaying(false);
  }, []);
  const videoData = useMemo(
    () => ({
      title: 'bunny movie',
      url: 'https://www.w3schools.com/html/mov_bbb.mp4',
    }),
    [],
  );
  return (
    <div>
      <input
        type="text"
        value={text}
        onChange={(e) => {
          setText(e.target.value);
        }}
      />
      <span>{text}</span>
      <div>
        Video is
        {isPlaying ? 'Playing' : 'Paused'}
      </div>
      <VideoPlayer
        // src={{
        //   title: '/bunnu movie',
        //   url: 'https://www.w3schools.com/html/mov_bbb.mp4',
        // }}
        src={videoData}
        onPlay={onPlay}
        onPause={onPause}
      />
    </div>
  );
}

export default Video;
